To run this example:

- make sure you have go/make/proto software installed
- run postgres somewhere with `postgres/postgres` credentials

To prepare database:

```
create database service;
\c service;
create table Record (
    Id integer primary key,
    FirstName character varying(255),
    LastName character varying(255),
    Email character varying(255),
    Phone character varying(255),
    Crm boolean default false not null
);
```

To build the project:
```
make install build
```

To run the integrator:
```
bin/integrator \
    -bind :7000 \
    -apitimeout 500 \
    -databasedsn postgres://postgres:postgres@localhost:5432/service?sslmode=disable \
    -verbose
```

Integrator is subscribed to OS' term signals and keyboard interrupt event.
Gracefull shutdown in effect. `apitimeout` flag is to set up throttling value
for api request retries.

To run the reader:
```
bin/reader \
    -integratoraddr 127.0.0.1:7000 \
    -filepath mock.csv \
    -timeout 50
```

Timeout in milliseconds is for timelapse between line reads.
`mock.csv` contains some broken rows which should be handled (say, skipped) properly.

Integrator expects solid connection to database and does not handle possible TCP issues.
