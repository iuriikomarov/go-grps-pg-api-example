package service

import (
	"google.golang.org/grpc"
	"log"
	"math/rand"
	"net"
	"os"
)

func Getenv(name string, fallback string) string {
	env := os.Getenv(name)
	if env == "" {
		env = fallback
	}
	return env
}

func RunGRPCServer(server *grpc.Server, bindTo string, label string) {
	l, err := net.Listen("tcp", bindTo)
	if err != nil {
		log.Fatalf("Failed to bind to TCP socket: %v", err)
	}
	log.Printf("%v is listening on %v", label, bindTo)
	server.Serve(l)
}

func CrmApiRequest(r *Record) bool {
	// This is mock function which does not send anything
	// but it returns false to emulate API bad response with 20% prob.
	x := rand.Intn(100)
	if x > 90 {
		return false
	}
	return true
}
