package service

import (
	"database/sql"
	"github.com/lib/pq"
)

type DBResult int

const (
	RecordCreated       DBResult = iota
	RecordAlreadyExists DBResult = iota
	RecordError         DBResult = iota
	RecordUpdated       DBResult = iota
)

func SaveRecord(r *Record, db *sql.DB) DBResult {
	sql := `
		INSERT INTO Record (Id, FirstName, LastName, Email, Phone)
		VALUES ($1, $2, $3, $4, $5)`
	_, err := db.Exec(sql, r.Id, r.FirstName, r.LastName, r.Email, r.Phone)

	if err == nil {
		return RecordCreated
	}

	pgerror := err.(*pq.Error)
	if pgerror.Code == "23505" {
		return RecordAlreadyExists
	}

	return RecordError
}

func MarkRecordAsSaved(r *Record, db *sql.DB) DBResult {
	sql := `UPDATE Record SET crm = true WHERE Id = $1`
	_, err := db.Exec(sql, r.Id)

	if err != nil {
		return RecordError
	}
	return RecordUpdated
}
