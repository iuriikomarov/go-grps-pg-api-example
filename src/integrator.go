package main

import (
	"context"
	"database/sql"
	"flag"
	"github.com/golang/protobuf/ptypes/empty"
	_ "github.com/lib/pq"
	"log"
	"os"
	"os/signal"
	"service"
	"syscall"
	"time"

	"google.golang.org/grpc"
)

var (
	emptyResponse *empty.Empty

	optionApiTimeout  int
	optionDatabaseDSN string
	optionBind        string
	optionVerbose     bool

	grpcServer *grpc.Server
	database   *sql.DB
)

type Service struct{}

func (s *Service) Send(ctx context.Context, r *service.Record) (*empty.Empty, error) {
	result := service.SaveRecord(r, database)
	switch result {
	case service.RecordCreated:
		go s.SendToCRM(r)
	case service.RecordAlreadyExists:
		if optionVerbose {
			log.Printf("Record with id %v already exists in Service database", r.Id)
		}
	case service.RecordError:
		log.Printf("Error saving record %v to Service database", r.Id)
	}
	return emptyResponse, nil
}

func (s *Service) SendToCRM(r *service.Record) {
	for {
		if service.CrmApiRequest(r) {
			result := service.MarkRecordAsSaved(r, database)
			if result == service.RecordError {
				log.Printf("Record %v is sent to CRM but not marked in Service database as sent.", r.Id)
			}
			return
		}
		if optionVerbose {
			log.Printf("Failed to send record %v to CRM api, will retry...", r.Id)
		}
		time.Sleep(time.Duration(optionApiTimeout) * time.Millisecond)
	}
}

func init() {
	emptyResponse = new(empty.Empty)

	flag.IntVar(
		&optionApiTimeout,
		"apitimeout",
		500,
		"retry failed CRM api request timeout, in milliseconds",
	)
	flag.StringVar(
		&optionBind,
		"bind",
		":7000",
		"bind integrator server to",
	)
	flag.StringVar(
		&optionDatabaseDSN,
		"databasedsn",
		"postgres://postgres:postgres@localhost:5432/service?sslmode=disable",
		"Service database DSN",
	)

	flag.BoolVar(
		&optionVerbose,
		"verbose",
		false,
		"log about failed API requests",
	)
	flag.Parse()

	grpcServer = grpc.NewServer()
	service.RegisterIntegratorServer(grpcServer, &Service{})
}

func main() {
	prepareDB()

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	go service.RunGRPCServer(grpcServer, optionBind, "Integrator")

	<-stop
	shutdown()
}

func shutdown() {
	log.Println("Gracefully shutting down Integrator...")
	time.Sleep(time.Second * 5)
	grpcServer.GracefulStop()
	database.Close()
}

func prepareDB() {
	report := func(err error) {
		log.Fatalf("Failed to connect to postgres: %v", err)
	}
	var err error
	database, err = sql.Open("postgres", optionDatabaseDSN)
	if err != nil {
		report(err)
	}
	err = database.Ping()
	if err != nil {
		report(err)
	}
}
