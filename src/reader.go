package main

import (
	"bufio"
	"context"
	"errors"
	"flag"
	"google.golang.org/grpc"
	"log"
	"os"
	"service"
	"strconv"
	"strings"
	"time"
)

var (
	optionIntegratorAddr string
	optionFilePath       string
	optionTimeout        int

	integratorConnection *grpc.ClientConn
	integratorClient     service.IntegratorClient
)

func init() {
	flag.StringVar(
		&optionIntegratorAddr,
		"integratoraddr",
		"127.0.0.1:7000",
		"integrator server addr",
	)
	flag.StringVar(
		&optionFilePath,
		"filepath",
		"mock.csv",
		"path to CSV data file",
	)
	flag.IntVar(
		&optionTimeout,
		"timeout",
		50,
		"pause execution before each line, in milliseconds",
	)
	flag.Parse()

	var err error
	integratorConnection, err = grpc.Dial(optionIntegratorAddr, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Failed to connect to integrator: %s", err)
	}
	integratorClient = service.NewIntegratorClient(integratorConnection)
}

func main() {
	file, err := os.Open(optionFilePath)
	if err != nil {
		log.Fatalf("Can not open file %v", optionFilePath)
	}

	defer file.Close()
	defer integratorConnection.Close()

	log.Printf("Start reading file %v...", optionFilePath)
	scanner, badLines := bufio.NewScanner(file), 0
	scanner.Scan()
	for scanner.Scan() {
		time.Sleep(time.Millisecond * time.Duration(optionTimeout))
		r, err := parseLine(scanner.Text())
		if err != nil {
			badLines = badLines + 1
			continue
		}
		_, err = integratorClient.Send(context.Background(), r)
		if err != nil {
			log.Println("Integrator is not responding. Reader execution is terminating earlier.")
			return
		}
	}

	if badLines > 0 {
		log.Printf("%v lines were not parsed.", badLines)
	}
	if err = scanner.Err(); err != nil {
		log.Printf("General error occured while reading file %v: %v", optionFilePath, err)
	}

	log.Printf("Finished.")
}

func parseLine(line string) (*service.Record, error) {
	s := strings.Split(line, ",")
	if len(s) != 5 {
		return nil, errors.New("Number of chunks is invalid")
	}
	id, err := strconv.ParseInt(s[0], 10, 32)
	if err != nil {
		return nil, err
	}
	return &service.Record{
		Id:        int32(id),
		FirstName: s[1],
		LastName:  s[2],
		Email:     s[3],
		Phone:     s[4],
	}, nil
}
