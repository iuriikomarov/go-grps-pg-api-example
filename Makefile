export GOPATH=$(PWD)

build: src bin

clean:
	rm -rf bin pkg src/service/protocol.pb.go

install:
	go get -u github.com/golang/protobuf/proto github.com/lib/pq google.golang.org/grpc

src: src/service/protocol.pb.go

src/service/protocol.pb.go: src/service/protocol.proto
	protoc $< --go_out=plugins=grpc:.

bin: bin/integrator bin/reader

bin/%: src/%.go
	go build -o $@ $<
